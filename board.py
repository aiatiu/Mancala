#This board represents a Mancala game
#For more on the rules of the game, please visit https://www.thespruce.com/how-to-play-mancala-409424

class Board(object):

    #static class variables - shared across all instances
    BUCKETS = 6

    ########################   Constructor   ###############################
    #
    #
    #  No arguments --> Creates a brand new empty board
    #
    #  orig         --> If you pass an existing board as the orig argument,
    #                   this will create a copy of that board
    #
    # *NOTE: orig and hash are mutually exclusive
    ########################################################################
    def __init__(self, orig=None):

        #copy
        if(orig):
            self.board = [i for i in orig.board]
            self.numMoves = orig.numMoves
            return

        #create new
        else:
            self.board = [4 for _ in range(self.BUCKETS * 2 + 2)]
            self.board[-1] = 0
            self.board[self.BUCKETS] = 0
            self.numMoves = 0
            return


    ########################################################################
    #                           Game Logic
    ########################################################################

    def makeMove(self, bucket):

        #needs to be a moveable buck (in bounds and not a store)
        if bucket == self.BUCKETS or bucket > self.BUCKETS * 2 or bucket < 0:
            return -1

        #needs to be a valid move for the player
        if self.numMoves % 2 == 0 and bucket > self.BUCKETS or self.numMoves % 2 == 1 and bucket < self.BUCKETS:
            return -1

        #pick up the stones
        stones = self.board[bucket]
        self.board[bucket] = 0


        #need to pick up at least one stone
        if stones == 0:
            return -2

        #deposit the stones
        offset = 0
        while(offset <  stones):
            bucketIndex = (bucket + offset + 1) % (self.BUCKETS * 2 + 2)

            #skip opponents store
                    #player one skips player twos store                                 #player 2 skips player ones store
            if((bucket < self.BUCKETS and bucketIndex == self.BUCKETS * 2  + 1) or (bucket > self.BUCKETS and bucketIndex == self.BUCKETS)):
                stones += 1

            # deposit a stone
            else:
                self.board[bucketIndex] += 1
            offset += 1

        #if the last move was in a bucket (not store) that didn't contain any stones and was on the player's side of the board,
        #add all the stones from this bucket and the opposite bucket to the player's store
        if self.board[bucketIndex] == 1 and bucketIndex != self.BUCKETS and bucketIndex != self.BUCKETS * 2 + 1:

            if bucket < self.BUCKETS and bucketIndex < self.BUCKETS:
                self.board[self.BUCKETS] += self.board[(2 * self.BUCKETS) - bucketIndex] + 1
                self.board[bucketIndex] = 0
                self.board[(2 * self.BUCKETS) - bucketIndex] = 0

            elif bucket > self.BUCKETS and bucketIndex > self.BUCKETS:
                self.board[-1] += self.board[(2 * self.BUCKETS) - bucketIndex] + 1
                self.board[bucketIndex] = 0
                self.board[(2 * self.BUCKETS) - bucketIndex] = 0



        #increment the number of moves made so far
        self.numMoves += 1
        return bucket


    # Generates a list of the valid children of the board
    # A child is of the form (move_to_make_child, child_object)
    def children(self):

        children = []

        if self.numMoves % 2 == 0:
            moves = range(self.BUCKETS)
        else:
            moves = range(self.BUCKETS + 1, self.BUCKETS * 2 + 2)

        child = Board(orig=self)
        for move in moves:

            move = child.makeMove(move)

            if(move >= 0):
                children.append((move, child))
                child = Board(orig=self)

        return children



    # Returns true iff the board is in a terminal position (player has no stones on their side)
    def isOver(self):

        over = True

        for i in range(self.BUCKETS):
            if self.board[i] != 0:
                over = False
                break

        #if found a non-zero on player 1's side, then check for all zeros on player 2 side
        if not over:
            for i in range(self.BUCKETS + 1, self.BUCKETS * 2 + 1):
                if self.board[i] != 0:

                    #found a non-zero on both sides
                    return False

            #didn't find a non-zero on player 2's side
            return True

        #otherwise, didn't find a non-zero on player 1's side, so the game is over
        else:
            return True



    #adds all the stones on each players sides and returns player1 - player2
    #precondition: only call after verifying that the board isOver
    def score(self):
        player1 = 0
        for i in range(self.BUCKETS + 1):
            player1 += self.board[i]

        player2 = 0
        for i in range(self.BUCKETS + 1, self.BUCKETS * 2 + 2):
            player2 += self.board[i]

        return player1 - player2

    ########################################################################
    #                           Utilities
    ########################################################################



    # Prints out a visual representation of the board
    # X's are 1's and 0's are 0s
    def print(self):
        print("\t",end="")
        for bucketIndex in range(self.BUCKETS * 2,self.BUCKETS, -1):
            print(str(self.board[bucketIndex]) + "\t", end="")
        print()

        print(str(self.board[-1]) + "\t" * (self.BUCKETS + 1) + str(self.board[self.BUCKETS]))

        print("\t", end="")
        for bucketIndex in range(0,self.BUCKETS):
            print(str(self.board[bucketIndex]) + "\t", end="")
        print()
        print()